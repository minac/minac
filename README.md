# About me

I was born and raised in [Niksar](https://en.wikipedia.org/wiki/Niksar), Turkey. My passion for programming began in high school, where I started scripting with ES5 and HTML. This early interest led me to pursue a degree in computer science engineering.

After finishing high school, I moved to Istanbul to study computer science engineering and simultaneously started my career as a software developer, working with PHP 4 and MySQL. Like many PHP developers, I created my own framework before discovering Ruby in 2011, which has been my favorite scripting language ever since.

In 2017, I relocated to Berlin, where I currently reside. 

Before joining GitLab, I focused on developing eventually consistent distributed systems, utilizing message brokers like Apache Kafka and Amazon SQS.

In addition to my work as a software developer, I am also a certified scuba diving instructor, an emergency first response instructor, and a technical diver.

I enjoy designing general architecture of systems, implementing abstractions, and solving hard technical challenges/bugs by diving deep even if it takes reading and debugging the source code of the language/VM or DBMS.

### Communicating with me

I prefer to communicate through the following methods, in order of preference:

- Gitlab `@` mentions
- Slack messages
- Sync zoom calls

Please note that I am not fond of strong opinions lacking substantial background, such as:

- "ActiveRecord callbacks are bad"
- "Database triggers are bad"
- "NoSQL databases are better"
- "Functional programming is better than OOP"

### Open source

#### Current projects

- [fast_json/schema](https://github.com/meinac/fast_json-schema): Aiming to write the fastest JSON schema validator for Ruby.
- [pry-shell](https://github.com/meinac/pry-shell): An extension for `pry`.

And some others [here](https://github.com/meinac).

#### Previous contributions

- [Ruby on Rails](https://contributors.rubyonrails.org/contributors/mehmet-emin-inac/commits): Mainly contributed for Rails 5.
- [Hadooken](https://github.com/tourlane/hadooken): A framework for distributed systems using Apache Kafka written by myself.
- [Redash](https://github.com/getredash/redash): Some contributions.

### Diving deep

Here I am diving deep, but this time not to solve a bug :)

![Diving Deep](https://gitlab.com/minac/minac/-/raw/main/diving.JPG)